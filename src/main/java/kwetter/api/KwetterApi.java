package kwetter.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KwetterApi
{
    public static void main(String[] args)
    {
        SpringApplication.run(KwetterApi.class, args);
    }

}