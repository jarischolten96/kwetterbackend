package kwetter.api.controller;

import kwetter.api.mapper.ObjectMapper;
import kwetter.api.microservices.jwtToken.JWTTokenService;
import kwetter.api.models.backend.Account;
import kwetter.api.models.backend.Tweet;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


@CrossOrigin
@RestController
@RequestMapping("kwetter_api/tweet")
@RequiredArgsConstructor
public class TweetController extends JWTTokenService {

    @GetMapping("/getTweetsByUser/{id}")
    public ResponseEntity<List<Tweet>> getTweetsByUser(@PathVariable("id") String id) throws Exception
    {
        List<Tweet> tweets = tweetService.getTweetsByUser(UUID.fromString(id));
        for (Tweet tweet : tweets) tweet.setUser(userService.getUserById(UUID.fromString(id)));
        return ResponseEntity.ok(tweets);
    }


    @PostMapping("/newTweet")
    public ResponseEntity newTweet(@RequestBody Tweet tweet) throws URISyntaxException, IOException {
        if (!cloudService.checkLength(tweet)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        else {
            tweetService.addTweet(tweet);
            return ResponseEntity.ok(200);
        }
    }

    @GetMapping("/testFunction")
    public ResponseEntity all() throws Exception
    {
        Tweet tweet = Tweet.builder()
                .id(UUID.randomUUID())
                .date(new Date())
                .likes(new ArrayList<>())
                .user(Account.builder().id(UUID.randomUUID()).userName("testacc").build())
                .text("Hoi deze string is veel te lang en zou false moeten terug geven!").build();

        return ResponseEntity.ok(cloudService.checkLength(tweet));
    }

}
