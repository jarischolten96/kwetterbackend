package kwetter.api.controller;

import kwetter.api.message_broker_configuration.RabbitConstraints;
import kwetter.api.models.backend.Log;
import kwetter.api.models.backend.LoggingLevel;
import kwetter.api.service_response_exception.ServiceUnauthorizedException;
import kwetter.api.service_response_exception.CredentialsNotOkayException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;

@Slf4j
@ControllerAdvice
@RequiredArgsConstructor
@Order(Ordered.HIGHEST_PRECEDENCE)
public class HTTPControllerAdvice implements RabbitConstraints {

    private final AmqpTemplate rabbitTemplate;

    @ExceptionHandler(ServiceUnauthorizedException.class)
    public ResponseEntity handleServiceUnauthorizedException(final ServiceUnauthorizedException e)
    {
        new Thread(() -> rabbitTemplate.convertAndSend(EXCHANGE_ADD_LOG, ROUTING_KEY, createLog(e, LoggingLevel.ERROR))).start();
        log.error(String.format("ServiceUnauthorizedException 400, %s at: %s", e.getMessage(), e.getStackTrace()[0]));
        e.printStackTrace();
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity handleRuntimeException(final RuntimeException e)
    {
        new Thread(() -> rabbitTemplate.convertAndSend(EXCHANGE_ADD_LOG, ROUTING_KEY, createLog(e, LoggingLevel.ERROR))).start();
        log.error(String.format("RuntimeException 500, %s at: %s", e.getMessage(), e.getStackTrace()[0]));
        e.printStackTrace();
        return ResponseEntity.status(500).build();
    }

    @ExceptionHandler(URISyntaxException.class)
    public ResponseEntity handleURISyntaxException(final URISyntaxException e)
    {
        new Thread(() -> rabbitTemplate.convertAndSend(EXCHANGE_ADD_LOG, ROUTING_KEY, createLog(e, LoggingLevel.ERROR))).start();
        e.printStackTrace();
        log.error(String.format("URISyntaxException 500, %s at: %s", e.getMessage(), e.getStackTrace()[0]));
        return ResponseEntity.status(500).build();
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity handleIOException(final IOException e)
    {
        new Thread(() -> rabbitTemplate.convertAndSend(EXCHANGE_ADD_LOG, ROUTING_KEY, createLog(e, LoggingLevel.ERROR))).start();
        e.printStackTrace();
        log.error(String.format("IOException 500, %s at: %s", e.getMessage(), e.getStackTrace()[0]));
        return ResponseEntity.status(500).build();
    }

    @ExceptionHandler(CredentialsNotOkayException.class)
    public ResponseEntity handleCredentialsNotOkayException(final CredentialsNotOkayException e)
    {
        new Thread(() -> rabbitTemplate.convertAndSend(EXCHANGE_ADD_LOG, ROUTING_KEY, createLog(e, LoggingLevel.ERROR))).start();
        log.error(String.format("CredentialsNotOkayException 400, %s at: %s", e.getMessage(), e.getStackTrace()[0]));
        e.printStackTrace();
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    private Log createLog(Exception e, LoggingLevel level)
    {
        return Log.builder()
                .content(e.getMessage())
                .time(new Date().getTime())
                .origin(e.getStackTrace()[0].getClassName())
                .level(level)
                .build();
    }

}
