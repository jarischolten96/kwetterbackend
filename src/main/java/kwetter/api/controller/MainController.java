package kwetter.api.controller;

import kwetter.api.factory.Factory;
import kwetter.api.microservices.cloud.ICloudFunctionService;
import kwetter.api.microservices.tweet.ITweetService;
import kwetter.api.microservices.user.IUserService;

public class MainController {
    IUserService userService = Factory.getUserService();
    ITweetService tweetService = Factory.getTweetService();
    ICloudFunctionService cloudService = Factory.getCloudService();
}
