package kwetter.api.controller;

import com.google.gson.Gson;
import kwetter.api.microservices.jwtToken.JWTTokenService;
import kwetter.api.models.backend.Account;
import kwetter.api.models.backend.UserCredentials;
import kwetter.api.service_response_exception.CredentialsNotOkayException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.net.URISyntaxException;

@CrossOrigin
@RestController
@RequestMapping("kwetter_api/user")
@RequiredArgsConstructor
public class UserController extends JWTTokenService {

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody UserCredentials credentials) throws URISyntaxException, IOException {
        userService.authenticate(credentials);
        Account account = userService.login(credentials);
        return ResponseEntity.ok(new Gson().toJson(createJWT(account)));
    }

    @PostMapping("/register")
    public ResponseEntity register(@RequestBody Account account) throws URISyntaxException, IOException {
        Account returnedAccount = userService.register(account);
        return ResponseEntity.ok(new Gson().toJson(createJWT(returnedAccount)));
    }

    /* Begin test functions */
    @GetMapping("/testRegister")
    public ResponseEntity<Account> testRegister() throws Exception{
        Account acc = Account.builder()
                .fullName("Jari Scholten")
                .userName("JariScholten")
                .password("TestPassword")
                .email("jari.scholten@gmail.com")
                .bio("Test biografie")
                .build();

        return ResponseEntity.ok(userService.register(acc));
    }

    @GetMapping("/testTweet")
    public ResponseEntity<Account> testTweet() throws Exception{
        Account acc = Account.builder()
                .fullName("Jari Scholten")
                .userName("JariScholten")
                .password("TestPassword")
                .email("jari.scholten@gmail.com")
                .bio("Test biografie")
                .build();

        return ResponseEntity.ok(userService.register(acc));
    }

    @GetMapping("/testAccount")
    public ResponseEntity<Account> testAccount() throws Exception{
        Account acc = Account.builder()
                .fullName("Jari Scholten")
                .userName("JariScholten")
                .password("TestPassword")
                .email("jari.scholten@gmail.com")
                .bio("Test biografie")
                .build();

        return ResponseEntity.ok(acc);
    }

    @GetMapping("/testLogger")
    public ResponseEntity testLogger() throws Exception{
        Boolean b = true;
        if (b) throw new CredentialsNotOkayException();
        System.out.println("Working testLogger");
        return ResponseEntity.ok(200);
    }

    /* End test functions */
}
