package kwetter.api.factory;

import kwetter.api.microservices.cloud.CloudFunctionService;
import kwetter.api.microservices.cloud.ICloudFunctionService;
import kwetter.api.microservices.tweet.ITweetService;
import kwetter.api.microservices.tweet.TweetService;
import kwetter.api.microservices.user.IUserService;
import kwetter.api.microservices.user.UserService;

public class Factory {
    public static ITweetService getTweetService()
    {
        return new TweetService() {
        };
    }
    public static IUserService getUserService()
    {
        return new UserService() {
        };
    }
    public static ICloudFunctionService getCloudService()
    {
        return new CloudFunctionService() {
        };
    }
}
