package kwetter.api.json_object;

import java.util.ArrayList;
import java.util.List;

public class JsonItemList<T> extends JsonObject
{
    private List<T> items;

    public JsonItemList()
    {
        super();
        this.items = new ArrayList<>();
    }

    public JsonItemList(String message, List<T> items)
    {
        super(message);
        this.items = items;
    }

    public List<T> getItems()
    {
        return items;
    }

    public void setItems(List<T> items)
    {
        this.items = items;
    }
}