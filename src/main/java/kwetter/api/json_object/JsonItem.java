package kwetter.api.json_object;

public class JsonItem<T> extends JsonObject
{
    private T item;

    public JsonItem(){
        super();
    }
    public JsonItem(String message, T item)
    {
        super(message);
        this.item = item;
    }


    public T getItem()
    {
        return item;
    }

    public void setItem(T item)
    {
        this.item = item;
    }
}