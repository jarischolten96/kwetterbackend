package kwetter.api.json_object;

public abstract class JsonObject
{
    private String message;

    JsonObject()
    {
        this.message = null;
    }

    JsonObject(String message)
    {
        this.message = message;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}