package kwetter.api.models.backend;

public enum LoggingLevel
{
    DEBUG,
    INFO,
    WARNING,
    ERROR,
    CRITICAL
}
