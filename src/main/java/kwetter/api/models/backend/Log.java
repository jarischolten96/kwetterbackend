package kwetter.api.models.backend;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Log {
    private UUID id;
    private Long time;
    private LoggingLevel level;
    private String origin;
    private String content;
}
