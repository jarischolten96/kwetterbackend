package kwetter.api.microservices.user;

import kwetter.api.microservices.MicroService;
import kwetter.api.models.backend.Account;
import kwetter.api.models.backend.UserCredentials;
import org.apache.http.client.utils.URIBuilder;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.UUID;

public class UserService extends MicroService implements IUserService {

    private static final String PATH = "/user";

    @Override
    public Account login(UserCredentials credentials) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder();
        builder.setScheme(HTTP).setHost(USER_SERVER).setPath(PATH + "/login").setPort(USER_PORT);
        return super.postItemWithResponse(builder, credentials, Account.class);
    }

    @Override
    public Account register(Account account) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder();
        builder.setScheme(HTTP).setHost(USER_SERVER).setPath(PATH + "/register").setPort(USER_PORT);
        return super.postItemWithResponse(builder, account, Account.class);
    }

    @Override
    public void authenticate(UserCredentials credentials) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder();
        builder.setScheme(HTTP).setHost(USER_SERVER).setPath(PATH + "/authenticate").setPort(USER_PORT);
        super.postItemWithResponse(builder, credentials, void.class);
    }

    @Override
    public Account getUserById(UUID id) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder();
        builder.setScheme(HTTP).setHost(USER_SERVER).setPath(PATH + "/getUserById/" + id.toString()).setPort(USER_PORT);
        return super.getItem(builder,Account.class);
    }


}
