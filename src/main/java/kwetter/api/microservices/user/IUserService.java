package kwetter.api.microservices.user;

import kwetter.api.models.backend.Account;
import kwetter.api.models.backend.UserCredentials;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.UUID;

public interface IUserService {
    Account login(UserCredentials credentials) throws URISyntaxException, IOException;
    Account register(Account account) throws URISyntaxException, IOException;
    void authenticate(UserCredentials credentials) throws URISyntaxException, IOException;
    Account getUserById(UUID id) throws URISyntaxException, IOException;
}
