package kwetter.api.microservices.tweet;

import kwetter.api.models.backend.Account;
import kwetter.api.models.backend.Tweet;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

public interface ITweetService {
    Tweet addTweet(Tweet tweet) throws URISyntaxException, IOException;
    List<Tweet> getTweetsByUser(UUID id) throws URISyntaxException, IOException;
}
