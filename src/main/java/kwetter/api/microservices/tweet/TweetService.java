package kwetter.api.microservices.tweet;

import com.fasterxml.jackson.core.type.TypeReference;
import kwetter.api.microservices.MicroService;
import kwetter.api.models.backend.Tweet;
import org.apache.http.client.utils.URIBuilder;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

public class TweetService extends MicroService implements ITweetService {

    private static final String PATH = "/tweet";

    @Override
    public Tweet addTweet(Tweet tweet) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder();
        builder.setScheme(HTTP).setHost(TWEET_SERVER).setPath(PATH + "/").setPort(TWEET_PORT);
        return super.postItemWithResponse(builder, tweet, Tweet.class);
    }

    @Override
    public List<Tweet> getTweetsByUser(UUID id) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder();
        builder.setScheme(HTTP).setHost(TWEET_SERVER).setPath(PATH + "/getTweetsByUserId/" + id.toString()).setPort(TWEET_PORT);
        return super.getList(builder,new TypeReference<List<Tweet>>(){}.getType());
    }


}
