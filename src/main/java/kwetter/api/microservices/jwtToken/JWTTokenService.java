package kwetter.api.microservices.jwtToken;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import kwetter.api.factory.Factory;
import kwetter.api.microservices.cloud.ICloudFunctionService;
import kwetter.api.microservices.tweet.ITweetService;
import kwetter.api.microservices.user.IUserService;
import kwetter.api.models.backend.Account;
import io.jsonwebtoken.SignatureAlgorithm;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;
import java.util.UUID;

public abstract class JWTTokenService implements IJWTTokenService
{
    private static final String SECRET_KEY = "sdg415fd#$TR#TGWE%Ytfg$%TGwvgtyieu5ngytviuwe5ngtyiue5nlwse5mbgysiuerv,h.lrdeihyiubes5vgmi%YT$%YTVysg4d5f4g6d";
    private static final int TIME_TO_LIVE = 1800000;
    private static final String FULLNAME = "fullName";
    private static final String BIO = "bio";
    private static final String USERNAME = "userName";
    private static final String USER_ID = "userId";
    private static final String EMAIL = "email";
    protected static final String NEW_TOKEN_HEADER = "newToken";
    public IUserService userService = Factory.getUserService();
    public ICloudFunctionService cloudService = Factory.getCloudService();
    public ITweetService tweetService = Factory.getTweetService();

    @Override
    public String createJWT(Account account)
    {
        Key signingKey = new SecretKeySpec(DatatypeConverter.parseBase64Binary(SECRET_KEY), SignatureAlgorithm.HS256.getJcaName());
        JwtBuilder builder = Jwts.builder()
                .setSubject("KwetterToken")
                .claim(USER_ID, account.getId())
                .claim(FULLNAME, account.getFullName())
                .claim(EMAIL, account.getEmail())
                .claim(USERNAME, account.getUserName())
                .claim(BIO,account.getBio())
                .setIssuer("Kwetter")
                .signWith(signingKey);
        if (TIME_TO_LIVE > 0) builder.setExpiration(new Date(System.currentTimeMillis() + TIME_TO_LIVE));
        return builder.compact();
    }

    @Override
    public Account decodeJWT(String jwt)
    {
        Account account = new Account();
        Claims claims;
        claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(SECRET_KEY)).parseClaimsJws(jwt.replace("Bearer", "").trim()).getBody();
        account.setEmail(claims.get(EMAIL).toString());
        account.setFullName(claims.get(FULLNAME).toString());
        account.setUserName(claims.get(USERNAME).toString());
        account.setBio(claims.get(BIO).toString());
        account.setId(UUID.fromString(claims.get(USER_ID).toString()));
        return account;
    }

    protected String generateNewToken(String jwtToken) {
        System.out.println("jwtToken heeft dit: " + jwtToken);
        Account account = decodeJWT(jwtToken);
        return createJWT(account);
    }
}
