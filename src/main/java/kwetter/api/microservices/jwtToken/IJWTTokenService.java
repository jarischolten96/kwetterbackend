package kwetter.api.microservices.jwtToken;

import kwetter.api.models.backend.Account;

public interface IJWTTokenService
{
    String createJWT(Account account);
    Account decodeJWT(String jwt);
}