package kwetter.api.microservices.cloud;

import kwetter.api.models.backend.Tweet;

import java.io.IOException;
import java.net.URISyntaxException;

public interface ICloudFunctionService {
    Boolean checkLength(Tweet tweet) throws URISyntaxException, IOException;
}
