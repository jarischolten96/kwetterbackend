package kwetter.api.microservices.cloud;

import kwetter.api.microservices.MicroService;
import kwetter.api.models.backend.Tweet;
import org.apache.http.client.utils.URIBuilder;

import java.io.IOException;
import java.net.URISyntaxException;

public class CloudFunctionService extends MicroService implements ICloudFunctionService
{
    @Override
    public Boolean checkLength(Tweet tweet) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder();
        builder.setScheme(HTTPS).setHost(CLOUD_SERVER).setPath("/api/httpexample");
        return super.postItemWithResponse(builder,tweet,Boolean.class);
    }
}
